package com.patcharawalai.app1hello

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.patcharawalai.app1hello.databinding.ActivityHelloactivityBinding

class Helloactivity : AppCompatActivity() {
private lateinit var binding: ActivityHelloactivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHelloactivityBinding.inflate(this.layoutInflater)
        val view = binding.root
        setContentView(view)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val name = intent?.extras?.getSerializable("name").toString()
        binding.namesurname.text = name
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
